import requests
import os
import json
from googletrans import Translator
from dotenv import load_dotenv

languagesCodes = ['kn', 'hi', 'bn', 'ml', 'mr', 'ta', 'te']


def getAuthToken():
    url = os.getenv("ENDPOINT") + os.getenv("AUTH_PATH")
    payload = {
        "grant_type": "password",
        "username": os.getenv("USER_NAME"),
        "password": os.getenv("PASSWORD"),
        "client_id": os.getenv("CLIENT_ID"),
        "client_secret": os.getenv("CLIENT_SECRET")
    }
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    print(payload)
    response = requests.post(
        url, headers=headers, json=payload)
    print(response.text)
    return json.loads(response.text)['access_token']

# passing auth bearer token for the request


class BearerAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["authorization"] = "Bearer " + self.token
        return r


def pushNewTranslatedFile(languagesCode):
    myfiles = {'file': open('translations/'+languagesCode+'.json', 'rb')}
    requests.post(os.getenv("ENDPOINT") + os.getenv("PROJECT_ID") + '/imports',
                  auth=BearerAuth(authBearerToken),
                  params={
                      "locale": languagesCode+"_IN",
                      "format": "jsonnested"
    },
        files=myfiles)


# get Translated data
def getCompleteTraslatedJson(languagesCodes):
    languagesCodes.append('en')
    for languagesCode in languagesCodes:
        res = requests.get(os.getenv("ENDPOINT") + os.getenv("PROJECT_ID") + '/exports',
                           auth=BearerAuth(authBearerToken),
                           params={
                               "locale": languagesCode + "_IN",
                               "format": "jsonflat",
                               "projectId": os.getenv("PROJECT_ID")
        })
        decodedFile = res.content.decode('utf-8')
        decodedJsonFile = json.loads(decodedFile)
        with open('translations/'+languagesCode+'.json', 'w', encoding='utf8') as writing:
            json.dump(decodedJsonFile, writing, indent=4, ensure_ascii=False)


# update tradoura with the translated text
def updateTranslatedTextValueonTradoura(langCode, termId, engText):
    try:
        trans = translator.translate(engText, src='en', dest=langCode).text
        data = {
            "termId": termId,
            "value": trans
        }
        requests.patch(os.getenv("ENDPOINT") + os.getenv("PROJECT_ID") + '/translations/' + langCode + '_IN',
                       auth=BearerAuth(authBearerToken), data=data)
    except Exception:
        pass


# generate the local langauge json file
def loadLocalLanguageJsonData(localeValue):
    localeTranslationData = requests.get(
        os.getenv("ENDPOINT") + os.getenv("PROJECT_ID") + '/translations/' + localeValue + '_IN', auth=BearerAuth(authBearerToken))

    if localeTranslationData.status_code == 200:
        decodedLocalTranslations = localeTranslationData.content.decode(
            'utf-8')
        decodedLocalJsonFormat = json.loads(decodedLocalTranslations)['data']
    return decodedLocalJsonFormat


def mainCaller():
    # terms file loaded
    termsData = requests.get(
        os.getenv("ENDPOINT") + os.getenv("PROJECT_ID") + '/terms', auth=BearerAuth(authBearerToken))
    pushNewTranslatedFile("en")

    # English language loaded
    englishTranslationData = requests.get(
        os.getenv("ENDPOINT") + os.getenv("PROJECT_ID") + '/translations/en_IN', auth=BearerAuth(authBearerToken))

    if englishTranslationData.status_code == 200:
        decodedEnglishTranslations = englishTranslationData.content.decode(
            'utf-8')
        englishJsonFormat = json.loads(decodedEnglishTranslations)['data']

    # if terms loaded, then start finding empty spaces and fill with translations
    if termsData.status_code == 200:
        decodedTermsResponse = termsData.content.decode('utf-8')
        termsJsonLoadedFormat = json.loads(decodedTermsResponse)['data']

        for languageCode in languagesCodes:
            decodedLocalLanguageJson = loadLocalLanguageJsonData(languageCode)
            for eachTerm in termsJsonLoadedFormat:
                for eachTranslation in decodedLocalLanguageJson:
                    if eachTerm['id'] == eachTranslation['termId']:
                        if eachTranslation['value'] == '':
                            for englishTerm in englishJsonFormat:
                                if eachTerm['id'] == englishTerm['termId']:
                                    updateTranslatedTextValueonTradoura(
                                        languageCode, eachTerm['id'], englishTerm['value'])

        getCompleteTraslatedJson(languagesCodes)


def translate():
    mainCaller()
    for languageCode in languagesCodes:
        pushNewTranslatedFile(languageCode)
    mainCaller()


# load the env file
load_dotenv()

# Load the translator service
translator = Translator()

# get the auth token
authBearerToken = getAuthToken()

# Translate values
translate()
